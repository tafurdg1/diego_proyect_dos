package mx.unam.fciencias.ejercicio1;

/**
 *
 * @author FRONT_END_1
 */
public class CuadroMagico {

    private static Integer[][] elementos;
    private static int tamano;

    public CuadroMagico(int tamano, boolean arriba) {
        int evaluacion = tamano % 2;
        if (evaluacion == 0) {
            tamano = 3;
            arriba = true;
        }
        this.tamano = tamano;
        this.elementos = new Integer[tamano][tamano];
        construir(arriba);

    }

    public Integer getElemento(int fila, int columna) {

        try {
            return elementos[fila][columna];
        } catch (Exception e) {
            return null;
        }

    }

    public int getTamaño() {
        return tamano;
    }

    public void setTamaño(int tamano, boolean arriba) {
        int evaluacion = tamano % 2;
        if (evaluacion != 0) {

            this.tamano = tamano;
            new CuadroMagico(tamano, arriba);

        }
    }

    private static void construir(boolean arriba) {

        int fAnt = 0;

        int cAnt = 0;

        int pInicial = tamano / 2;

        int temp = 1;

        for (int f = 0; f < tamano; f++) {
            for (int c = 0; c < tamano; c++) {
                elementos[f][c] = 0;
            }
        }
        int f = 0;
        int c = pInicial;
        if (!arriba) {
            f = tamano - 1;
        }
        while (temp != (tamano * tamano) + 1) {
            if (elementos[f][c] == 0) {
                elementos[f][c] = temp;
            } else {
                if (arriba) {

                    f = fAnt + 1;
                    c = cAnt;
                    elementos[f][c] = temp;
                } else {

                    f = fAnt - 1;
                    c = cAnt;
                    elementos[f][c] = temp;
                }
            }

            fAnt = f;
            cAnt = c;

            temp++;
            if (arriba) {

                c++;
                f--;
            } else {
                f++;
                c++;
            }
            if (arriba) {

                if (f < 0 && c == tamano) {
                    f = tamano - 1;
                    c = 0;

                } else if (f < 0) {
                    f = f + tamano;
                } else if (c == tamano) {
                    c = 0;
                }
            } else {
                if (f == tamano && c == tamano) {
                    f = 0;
                    c = 0;

                } else if (f >= tamano) {
                    f = f - tamano;
                } else if (c == tamano) {
                    c = 0;
                }

            }
        }
        for (int i = 0; i < tamano; i++) {
            String resultado = "";
            for (int j = 0; j < tamano; j++) {
                resultado = resultado + " " + elementos[i][j];
            }
            System.out.println(" " + resultado);
        }
    }

    @Override
    public String toString() {
        return "CuadroMagico{" + "elementos=" + elementos + ", tama\u00f1o=" + tamano + '}';
    }

}
