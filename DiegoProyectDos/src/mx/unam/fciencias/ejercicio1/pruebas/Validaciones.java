/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.unam.fciencias.ejercicio1.pruebas;

import java.util.Scanner;
import mx.unam.fciencias.ejercicio1.CuadroMagico;

/**
 *
 * @author JoseAngelPerezMolina
 */
public class Validaciones {

    public static void main(String[] args) {

        
        Scanner lectura = new Scanner(System.in);
        boolean direccion = true;
        System.out.println("Digite el tamaño de su matriz");
        int tamano = lectura.nextInt();
        
        System.out.println("Digite 1.Para Arriba o 2.Para Abajo");        
        int arriba = lectura.nextInt();
        
        if (arriba == 1 ||  arriba != 2) {
            direccion = true;
        } else if (arriba == 2) {
            direccion = false;

        }

        CuadroMagico c = new CuadroMagico(tamano, direccion);
        
        System.out.println("Elemento: " + c.getElemento(4, 1));
        System.out.println("Tamaño:" + c.getTamaño());
        c.setTamaño(3, direccion);
        
    }
}
