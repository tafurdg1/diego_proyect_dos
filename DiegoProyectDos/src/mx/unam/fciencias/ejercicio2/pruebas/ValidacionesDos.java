package mx.unam.fciencias.ejercicio2.pruebas;

import mx.unam.fciencias.ejercicio2.ComparadorCursos;
import mx.unam.fciencias.ejercicio2.Curso;
import mx.unam.fciencias.ejercicio2.Hora;
import mx.unam.fciencias.ejercicio2.Horario;
import mx.unam.fciencias.ejercicio2.Materia;

public class ValidacionesDos {

    public static void main(String[] args) {

        //Arreglo profesores
        String[] nombres = {"Andres", "Julian", "Angel"};
        int cantidadCurso = 10;
        Curso[] arregloCurso = new Curso[cantidadCurso];

        for (int i = 0; i < cantidadCurso; i++) {

            int aleatorio = (int) (Math.random() * (3) + 1);
            int aleatorio1 = (int) (Math.random() * (3) + 1);

            Hora horaI = new Hora((int) (Math.random() * (23) + 1), (int) (Math.random() * (59) + 1));
            Hora horaF = new Hora((int) (Math.random() * (23) + 1), (int) (Math.random() * (59) + 1));

            Horario horario = new Horario(horaI, horaF);

            Materia materia = new Materia(new Materia().getNombreMateria()[aleatorio - 1]);
            Curso curso = new Curso(materia, nombres[aleatorio1 - 1], horario, (int) (Math.random() * (10) + 1));

            arregloCurso[i] = curso;
        }

        ComparadorCursos comparador = new ComparadorCursos(arregloCurso);

        Hora horaI = new Hora(7, 00);
        Hora horaF = new Hora(17, 00);

        Horario horarioConsulta = new Horario(horaI, horaF);

        Hora horaIv = new Hora(17, 00);
        Hora horaFv = new Hora(7, 00);

        Horario horarioConsultaInvalida = new Horario(horaIv, horaFv);
        for (int i = 0; i < arregloCurso.length; i++) {
            System.out.println(arregloCurso[i].toString());
        }

        System.out.println("\n************************ CONSULTA COMPARATIVA *******************************");
        System.out.println(" ************************* VALORES VALIDOS *********************************");
        System.out.println("\nConsulta por horario\n");
        comparador.mostrarPorHorario(horarioConsulta);
        System.out.println("\nConsulta por dificultad\n");
        comparador.mostrarPorDificultad(5, 10);
        System.out.println("\nConsulta por materia\n");
        comparador.mostrarPorMateria("Programación");

        System.out.println("\n ************************* VALORES NO VALIDOS *********************************");
        System.out.println("\nConsulta por horario\n");
        comparador.mostrarPorHorario(horarioConsultaInvalida);
        System.out.println("\nConsulta por dificultad\n");
        comparador.mostrarPorDificultad(15, 5);
        System.out.println("\nConsulta por materia\n");
        comparador.mostrarPorMateria("Electricidad");
    }
}
