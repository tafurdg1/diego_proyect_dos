package mx.unam.fciencias.ejercicio2;

public class Horario {

    Hora horaInicio;
    Hora horaFin;

    public Horario(Hora horaInicio, Hora horaFin) {
        if (horaFin.getHora() < horaInicio.getHora()) {
            System.out.println("La hora fin no puede ser menor a la de inicio");
            this.horaFin = new Hora(0, 0);
            this.horaInicio = horaInicio;
        } else {
            this.horaInicio = horaInicio;
            this.horaFin = horaFin;
        }
    }

    public static Boolean estaEnRango(Hora horaInicio, Hora horaFin) {

        return true;
    }

    @Override
    public String toString() {
        return "hora inicio: " + horaInicio
                + " hora fin:" + horaFin;
    }

}
