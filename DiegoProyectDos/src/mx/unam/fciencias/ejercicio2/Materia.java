package mx.unam.fciencias.ejercicio2;

public class Materia {

    private static final String[] nombreMaterias = {"THC", "Programación",
        "Manejo de datos"};

    String nombre;

    public Materia() {

    }

    public Materia(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        for (int i = 0; i < nombreMaterias.length; i++) {
            if (!nombre.equals(nombreMaterias[i])) {
                System.out.println("Esta materia no existe");
                this.nombre = "Programación";
            } else {
                this.nombre = nombre;
            }
        }
    }

    public String[] getNombreMateria() {
        return this.nombreMaterias;
    }

    @Override
    public String toString() {
        return "Materia{" + "nombreMaterias=" + nombreMaterias + ", nombre=" + nombre + '}';
    }

}
