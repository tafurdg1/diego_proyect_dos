package mx.unam.fciencias.ejercicio2;

public class Curso {

    Materia materia;
    String profesor;
    Horario horario;
    int dificultad;

    public Curso(Materia materia, String profesor, Horario horario, int dificultad) {
        this.materia = materia;
        this.profesor = profesor;
        this.horario = horario;
        if (dificultad < 0 || dificultad > 10) {
            System.out.println("La dificultad es 0 ó 10");
            this.dificultad = 0;
        } else {
            this.dificultad = dificultad;
        }
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public String getProfesor() {
        return profesor;
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public int getDificultad() {
        return dificultad;
    }

    public void setDificultad(int dificultad) {
        if (dificultad < 0 || dificultad > 10) {
            System.out.println("La dificultad es 0 ó 10");
            this.dificultad = 0;
        } else {
            this.dificultad = dificultad;
        }
    }

    @Override
    public String toString() {
        return "Curso: \n" + "Materia: " + materia.nombre + "\n"
                + "Profesor: " + profesor
                + "\nHorario: " + horario.toString() + "\nDificultad: " + dificultad;
    }

}
