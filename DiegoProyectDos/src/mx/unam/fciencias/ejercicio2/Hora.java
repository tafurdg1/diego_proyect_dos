package mx.unam.fciencias.ejercicio2;

public class Hora {

    int hora;
    int minuto;

    public Hora(int hora) {
        this.hora = hora;
    }

    public Hora(int hora, int minuto) {
        if (hora > 23 || minuto > 59) {
            System.out.println("esta hora no es correcta");
        } else {
            this.hora = hora;
            this.minuto = minuto;
        }
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        if (hora > 23) {
            System.out.println("esta hora no es correcta");
        } else {
            this.hora = hora;
        }
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        if (minuto > 59) {
            System.out.println("Estos minutos no son correctos");
        } else {
            this.minuto = minuto;
        }
    }

    @Override
    public String toString() {
        String ini = "";
        String minIni = "";
        if (String.valueOf(hora).length() == 1) {
            ini = "0" + hora;
        } else {
            ini = String.valueOf(hora);
        }

        if (String.valueOf(minuto).length() == 1) {
            minIni = "0" + minuto;
        } else {
            minIni = String.valueOf(minuto);
        }

        return String.format(ini + ":" + minIni);
    }

}
