package mx.unam.fciencias.ejercicio2;

public class ComparadorCursos {

    public static Curso[] curso;

    public ComparadorCursos(Curso[] curso) {
        this.curso = curso;
    }

    public static void mostrarPorDificultad(int dificultadBaja, int dificultadAlta) {
        int temp = 0;
        Curso[] consulta = consultarPorDificultad(dificultadBaja, dificultadAlta);

        if (consultarPorDificultad(dificultadBaja, dificultadAlta) == null) {
            System.out.println("No se encontraron resultados");
        } else {
            for (int i = 0; i < consulta.length; i++) {
                if (consulta[i] != null) {
                    System.out.println(consulta[i].toString());
                    temp++;
                }
            }
        }

        if (temp == 0) {
            System.out.println("No se encontraron resultados");
        }
    }

    public static void mostrarPorHorario(Horario horario) {
        int temp = 0;
        Curso[] consulta = consultarPorHorario(horario);

        if (consulta == null) {
            System.out.println("No se encontraron resultados");
        } else {
            for (int i = 0; i < consulta.length; i++) {
                if (consulta[i] != null) {
                    System.out.println(consulta[i].toString());
                    temp++;
                }
            }
        }

        if (temp == 0) {
            System.out.println("No se encontraron resultados");
        }
    }

    public static void mostrarPorMateria(String nombreMateria) {
        int temp = 0;
        Curso[] consulta = consultarPorMateria(nombreMateria);

        if (consultarPorMateria(nombreMateria) == null) {
            System.out.println("No se encontraron resultados");
        } else {
            for (int i = 0; i < consulta.length; i++) {
                if (consulta[i] != null) {
                    System.out.println(consulta[i].toString());
                    temp++;
                }
            }
        }

        if (temp == 0) {
            System.out.println("No se encontraron resultados");
        }
    }

    public static Curso[] consultarPorHorario(Horario horario) {
        Curso[] cursoConsulta = curso;
        for (int i = 0; i < curso.length; i++) {
            if (curso[i] != null) {

                int horaIniCurso = curso[i].horario.horaInicio.hora;
                int horaFinCurso = curso[i].horario.horaFin.hora;
                int minutoIniCurso = curso[i].horario.horaInicio.minuto;
                int minutoFinCurso = curso[i].horario.horaFin.minuto;

                int horaHorarioIni = horario.horaInicio.hora;
                int horaHorarioFin = horario.horaFin.hora;
                int minutoHorarioIni = horario.horaInicio.minuto;
                int minutoHorarioFin = horario.horaFin.minuto;

                if ((horaIniCurso >= horaHorarioIni
                        && horaIniCurso <= horaHorarioFin)
                        && (horaFinCurso >= horaHorarioIni && horaFinCurso <= horaHorarioFin)) {
                    if (minutoIniCurso >= minutoHorarioIni) {
                        if (horaFinCurso == horaHorarioFin) {
                            if (minutoFinCurso <= minutoHorarioFin) {
                                cursoConsulta[i] = curso[i];
                            }
                        } else {
                            cursoConsulta[i] = curso[i];
                        }
                    } else {
                        cursoConsulta[i] = null;
                    }
                } else {
                    cursoConsulta[i] = null;
                }

            }
        }
        return cursoConsulta;
    }

    public static Curso[] consultarPorDificultad(int dificultadBaja, int dificultadAlta) {
        Curso[] cursoConsulta = curso;
        for (int i = 0; i < curso.length; i++) {
            if (curso[i] != null) {

                if (dificultadBaja < dificultadAlta) {
                    if (curso[i].getDificultad() > dificultadBaja && curso[i].getDificultad() < dificultadAlta) {
                        cursoConsulta[i] = curso[i];
                    }
                }
            }
        }

        return cursoConsulta;
    }

    public static Curso[] consultarPorMateria(String nombreMateria) {
        Curso[] cursoConsulta = curso;
        for (int i = 0; i < curso.length; i++) {
            if (curso[i] != null) {
                if (curso[i].getMateria().nombre.equals(nombreMateria)) {
                    cursoConsulta[i] = curso[i];
                }
            }
        }
        return cursoConsulta;
    }

    public static Curso[] getCursos() {

        return curso;
    }
}
